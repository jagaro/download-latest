import nox

LOCAL_LINT_PATHS = ("download_latest", "tests")
VENV_LINT_PATHS = LOCAL_LINT_PATHS + ("no-pip/download-latest",)


def lint_checks(session, paths):
    session.run("mypy", *paths)
    session.run("ruff", *paths)
    session.run("black", "--check", "--diff", *paths)


@nox.session(python=False)  # False = Use Poetry environment
def lint(session):
    lint_checks(session, LOCAL_LINT_PATHS)


@nox.session(python=["3.7", "3.12"])
def venv_lint(session):
    session.run("no-pip/generate", external=True)
    session.run(
        "poetry",
        "export",
        "--with",
        "dev",
        "--without-hashes",
        "--output",
        ".nox/requirements.txt",
        external=True,
    )
    session.install("-r", ".nox/requirements.txt")
    lint_checks(session, VENV_LINT_PATHS)


@nox.session(python=["3.7", "3.8", "3.9", "3.10", "3.11", "3.12"])
def test(session):
    session.install("pytest", "pytest-httpserver", "pytest-mock")
    session.install("--editable", ".")
    session.run("pytest")
    session.run("download-latest", "--version", external=True)
    session.run("dl", "--version", external=True)
