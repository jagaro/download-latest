## No-Pip Version

Alternatively, you can install a standalone version that does not require Pip.

### Install

```sh
sudo curl -sSfo /usr/local/bin/download-latest \
  https://gitlab.com/jagaro/download-latest/-/raw/main/no-pip/download-latest
sudo chmod 755 /usr/local/bin/download-latest
sudo ln -sf download-latest /usr/local/bin/dl
```
