# Download Latest

`download-latest` requires [Python >=3.7](https://www.python.org/downloads/).
Optionally, [cURL](https://curl.se/download.html) or
[Wget](https://www.gnu.org/software/wget/) can be used as backends if available.

## Python Install Guide

Instructions for installing Python 3 and Pip.

### Debian / Ubuntu

```sh
sudo apt update
sudo apt install -y python3 python3-pip
```

When installing a package using system `pip`, you may get the following error:

```
error: externally-managed-environment
× This environment is externally managed
╰─> To install Python packages system-wide, try apt install
    python3-xyz, where xyz is the package you are trying to
    install.
...
```

You can override this by specifying the `--break-system-packages` option.

### Fedora / RHEL

```sh
sudo dnf install -y python3 python3-pip
```

### Arch

```sh
sudo pacman -Syy
sudo pacman -Su --needed --noconfirm python python-pip
```

### Alpine

```sh
sudo apk add python3 py3-pip
```

### FreeBSD

```sh
pkg install -y python3
pkg install -xy '^py3.+-pip$'
```

### OpenBSD

```sh
doas pkg_add -z python3 py3-pip
```
