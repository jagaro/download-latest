from __future__ import annotations

from collections import namedtuple
import argparse
import logging
import pytest
from typing import Callable, Tuple

from download_latest.console import Console, ConsoleFormatter
from download_latest.cls import DownloadLatestException

from helpers import *  # noqa: F403


#
# Console.get_args()
#

GetArgsResult = namedtuple("GetArgsResult", ["args", "code", "stdout", "stderr"])


@pytest.fixture
def get_args(capsys) -> Callable:
    """Wraps console.get_args() and its dependents."""

    def get_args(args: list, isatty: bool) -> GetArgsResult:
        """Calls console.get_args() and returns a GetArgsResult."""
        code = 0
        parsed_args = None
        try:
            parsed_args = Console.get_args(args=args, isatty=isatty)
        except SystemExit as e:
            code = e.code if isinstance(e.code, int) else -1
        std = capsys.readouterr()
        return GetArgsResult(
            code=code,
            stdout=std.out,
            stderr=std.err,
            args=parsed_args,
        )

    return get_args


def test_console_get_args_basic(get_args):
    r = get_args(args=["a"], isatty=False)
    assert r.code == 0
    assert r.stdout == ""
    assert r.stderr == ""
    assert r.args.url == "a"
    assert r.args.file is None
    assert r.args.color is False
    assert r.args.dry_run is False
    assert r.args.force is False
    assert r.args.progress is False
    assert r.args.quiet is False
    assert r.args.verbose is False

    r = get_args(args=["a", "b"], isatty=False)
    assert r.code == 0
    assert r.stdout == ""
    assert r.stderr == ""
    assert r.args.url == "a"
    assert r.args.file == "b"


def test_console_get_args_help(get_args):
    r = get_args(args=["--help"], isatty=False)
    assert r.code == 0
    assert "positional arguments" in r.stdout
    assert r.stderr == ""


def test_console_get_args_mutually_exclusive(get_args):
    r = get_args(args=["a", "b", "-qv"], isatty=False)
    assert r.code == 2
    assert r.stdout == ""
    assert "not allowed" in r.stderr

    r = get_args(args=["a", "b", "--color", "--no-color"], isatty=False)
    assert r.code == 2
    assert r.stdout == ""
    assert "not allowed" in r.stderr

    r = get_args(args=["a", "b", "--progress", "--no-progress"], isatty=False)
    assert r.code == 2
    assert r.stdout == ""
    assert "not allowed" in r.stderr


def test_console_get_args_intermixed(get_args):
    r = get_args(
        args=["--dry-run", "xyz.com", "-vpv", "foo/bar", "--force"],
        isatty=False,
    )
    assert r.code == 0
    assert r.stdout == ""
    assert r.stderr == ""
    assert r.args.url == "xyz.com"
    assert r.args.file == "foo/bar"
    assert r.args.color is False
    assert r.args.dry_run is True
    assert r.args.force is True
    assert r.args.progress is True
    assert r.args.quiet is False
    assert r.args.verbose is True


def test_console_get_args_no_arguments(get_args):
    r = get_args(args=[], isatty=False)
    assert r.args is None
    assert r.code == 2
    assert r.stdout == ""
    assert "arguments are required" in r.stderr


def test_console_get_args_tty(get_args):
    r = get_args(args=["abc", "123"], isatty=True)
    assert r.code == 0
    assert r.stdout == ""
    assert r.stderr == ""
    assert r.args.url == "abc"
    assert r.args.file == "123"
    assert r.args.color is True
    assert r.args.dry_run is False
    assert r.args.force is False
    assert r.args.progress is True
    assert r.args.quiet is False
    assert r.args.verbose is False


def test_console_get_args_unknown_option(get_args):
    r = get_args(args=["-x", "abc"], isatty=False)
    assert r.code == 2
    assert r.stdout == ""
    assert "unrecognized arguments" in r.stderr
    assert r.args is None


def test_console_get_args_version(get_args, mocker):
    mocker.patch("download_latest.console.__version__", "1.2.3")
    r = get_args(args=["--version"], isatty=False)
    assert r.code == 0
    assert r.stdout == "download-latest 1.2.3\n"
    assert r.stderr == ""
    assert r.args is None


#
# Console.get_logger()
#


def test_console_get_logger_normal(capsys):
    logger = Console.get_logger(
        quiet=False, verbose=False, color=False, name="test_console_get_logger_normal"
    )
    logger.info("hi")
    logger.debug("boo")
    logger.warning("uh oh")
    std = capsys.readouterr()
    assert std.out == "hi\n"
    assert std.err == "uh oh\n"


def test_console_get_logger_quiet_and_verbose(capsys):
    logger = Console.get_logger(
        quiet=True,
        verbose=True,
        color=False,
        name="test_console_get_logger_quiet_and_verbose",
    )
    logger.info("hi")
    logger.debug("boo")
    logger.warning("uh oh")
    std = capsys.readouterr()
    assert std.out == ""
    assert std.err == ""


def test_console_get_logger_verbose(capsys):
    logger = Console.get_logger(
        quiet=False, verbose=True, color=False, name="test_console_get_logger_verbose"
    )
    logger.info("hi")
    logger.debug("boo")
    logger.warning("uh oh")
    std = capsys.readouterr()
    assert std.out == "hi\nboo\n"
    assert std.err == "uh oh\n"


#
# Console.main()
#


@pytest.fixture
def main(caplog, logger, mocker) -> Callable:
    """Wraps console.main() and its dependents."""

    def _main(side_effect: object) -> Tuple[int, str, Exception | None]:
        """Calls console.main() and returns a tuple (return code, log text)."""

        MockDL = mocker.patch("download_latest.console.DownloadLatest")
        dl = MockDL.return_value
        dl.run.side_effect = side_effect

        ns = argparse.Namespace(
            **{
                "url": None,
                "file": None,
                "backend": "auto",
                "color": False,
                "dry_run": False,
                "force": False,
                "progress": False,
                "quiet": False,
                "verbose": False,
            }
        )
        get_args = mocker.patch.object(Console, "get_args", return_value=ns)
        get_logger = mocker.patch.object(Console, "get_logger", return_value=logger)

        code = 0
        uncaught = None
        try:
            Console.main()
        except SystemExit as e:
            code = e.code if isinstance(e.code, int) else -1
        except Exception as e:
            code = 1
            uncaught = e

        get_args.assert_called_once()
        get_logger.assert_called_once()
        MockDL.assert_called_once()
        dl.run.assert_called_once()

        return (code, caplog.text, uncaught)

    return _main


def test_console_main_basic(main):
    code, log, uncaught = main(side_effect=None)
    assert code == 0
    assert log == ""
    assert uncaught is None


def test_console_main_download_latest_console_exception(main):
    code, log, uncaught = main(side_effect=DownloadLatestException("uh oh"))
    assert code == 1
    assert "ERROR" in log
    assert "uh oh" in log
    assert uncaught is None


def test_console_main_keyboard_interrupt(main):
    code, log, uncaught = main(side_effect=KeyboardInterrupt())
    assert code == 130
    assert "WARNING" in log
    assert "keyboard interrupt" in log
    assert uncaught is None


def test_console_main_keyboard_uncaught_exception(main):
    e = Exception("boom")
    code, log, uncaught = main(side_effect=e)
    assert code == 1
    assert "CRITICAL" in log
    assert "boom" in log
    assert uncaught == e


def test_console_main_os_error(main):
    code, log, uncaught = main(
        side_effect=PermissionError("[Errno 13] Permission denied: 'a'")
    )
    assert code == 1
    assert "error: [Errno 13]" in log
    assert "Permission denied" in log
    assert uncaught is None


#
# ConsoleFormatter
#

# fmt: off
FORMATTER_NORMAL_TYPES = [
    # levelno          color  msg
    (logging.DEBUG,    False, "hi1"),
    (logging.INFO,     False, "hi2"),
    (logging.WARNING,  False, "hi3"),
    (logging.ERROR,    False, "hi4"),
    (logging.CRITICAL, False, "hi5"),
    (logging.DEBUG,    True,  "hi1"),
    (logging.INFO,     True,  "hi2"),
    (logging.WARNING,  True,  "hi3"),
    (logging.ERROR,    True,  "hi4"),
    (logging.CRITICAL, True,  "hi5"),
]

FORMATTER_SUCCESS_TYPES = [
    (logging.DEBUG,    False, " success!"),
    (logging.INFO,     False, " success!"),
    (logging.WARNING,  False, " success!"),
    (logging.ERROR,    False, " success!"),
    (logging.CRITICAL, False, " success!"),
    (logging.DEBUG,    True,  " success!"),
    (logging.INFO,     True,  " success!"),
    (logging.WARNING,  True,  " success!"),
    (logging.ERROR,    True,  " success!"),
    (logging.CRITICAL, True,  " success!"),
]
# fmt: on


@pytest.mark.parametrize("levelno,color,msg", FORMATTER_NORMAL_TYPES)
def test_console_formatter_normal(levelno, color, msg):
    record = logging.LogRecord("a", levelno, None, 0, msg, None, None)
    output = ConsoleFormatter(color=color).format(record)
    if color:
        assert "\x1b" in output
    else:
        assert "\x1b" not in output


@pytest.mark.parametrize("levelno,color,msg", FORMATTER_SUCCESS_TYPES)
def test_console_formatter_success(levelno, color, msg):
    record = logging.LogRecord("a", levelno, None, 0, msg, None, None)
    output = ConsoleFormatter(color=color).format(record)
    if color:
        assert "\x1b[0;32m" in output  # green!
    else:
        assert "\x1b" not in output
