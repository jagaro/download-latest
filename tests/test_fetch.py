from __future__ import annotations

import http.client
import logging
import pytest

from download_latest.fetch import (
    Fetch,
    FetchCurl,
    FetchCurlDownloadTask,
    FetchCurlHeadTask,
    FetchParseException,
    FetchPython,
    FetchPythonDownloadTask,
    FetchPythonHeadTask,
    FetchResponse,
    FetchSubProcess,
    FetchWget,
    FetchWgetDownloadTask,
    FetchWgetHeadTask,
)

from download_latest.task import TaskUncaughtError

from helpers import *  # noqa: F403


@pytest.fixture(autouse=True)
def set_caplog_level_to_debug(caplog):
    caplog.set_level(logging.DEBUG)


#
# FetchResponse
#

SIMPLE_RAW_FETCH_RESPONSE = b"""\
HTTP/1.1 200 OK\r
Content-Type: text/plain\r
\r
"""


REDIRECT_RAW_FETCH_RESPONSE = b"""\
HTTP/1.1 301 Moved Permanently\r
Location: http://example.org/foo/bar\r
\r
HTTP/2 301 \r
location: ../baz\r
\r
HTTP/2 200 \r
\r
"""

FTP_RAW_FETCH_RESPONSE = b"""\
Last-Modified: Sun, 26 Jul 2015 04:32:50 GMT\r
Content-Length: 26\r
Accept-ranges: bytes\r
"""


ERROR_RAW_FETCH_RESPONSE = b"""\
HTTP/1.1 302 Found\r
Location: http://www.example.com/sorry/\r
\r
HTTP/1.1 405 Method Not Allowed\r
\r
"""

MALFORMED_RAW_FETCH_RESPONSE = b"""\
HTTP/1.0 BLAH!\r
1234567\r
"""

#
# FetchResponse.make_from_raw_headers()
#


def test_fetch_response_make_from_raw_headers_malformed():
    with pytest.raises(FetchParseException, match=r"cannot parse response$"):
        FetchResponse.make_from_raw_headers(
            url="what.kind.of.website.is.this",
            raw_headers=MALFORMED_RAW_FETCH_RESPONSE,
        )


def test_fetch_response_make_from_raw_headers_ok():
    response = FetchResponse.make_from_raw_headers(
        url="https://example.com",
        raw_headers=SIMPLE_RAW_FETCH_RESPONSE,
    )
    assert response.status == 200
    assert response.reason == "OK"
    assert response.headers["content-type"] == "text/plain"
    assert response.url == "https://example.com"


#
# FetchResponse.make_from_raw_multiple_headers()
#


def test_fetch_response_make_from_raw_multiple_headers_redirect():
    responses = FetchResponse.make_from_raw_multiple_headers(
        url="example.com",
        raw_multiple_headers=REDIRECT_RAW_FETCH_RESPONSE,
    )
    assert len(responses) == 3
    assert responses[0].status == 301
    assert responses[0].url == "example.com"
    assert responses[1].status == 301
    assert responses[1].url == "http://example.org/foo/bar"
    assert responses[2].status == 200
    assert responses[2].url == "http://example.org/baz"


def test_fetch_response_make_from_raw_multiple_headers_ftp():
    responses = FetchResponse.make_from_raw_multiple_headers(
        url="ftp://example.net/a.txt",
        raw_multiple_headers=FTP_RAW_FETCH_RESPONSE,
    )
    assert len(responses) == 1
    assert responses[0].status == 200
    assert responses[0].url == "ftp://example.net/a.txt"


def test_fetch_response_make_from_raw_multiple_headers_error():
    responses = FetchResponse.make_from_raw_multiple_headers(
        url="example.com",
        raw_multiple_headers=ERROR_RAW_FETCH_RESPONSE,
    )
    assert len(responses) == 2
    assert responses[0].status == 302
    assert responses[0].url == "example.com"
    assert responses[1].status == 405
    assert responses[1].url == "http://www.example.com/sorry/"


#
# Fetch
#

#
# Fetch.__init__()
#


def test_fetch_init(logger):
    fetch = Fetch(logger=logger, meter=True)
    assert fetch.logger is logger
    assert fetch.meter is True
    with pytest.raises(NotImplementedError):
        fetch.download("a", "b")
    with pytest.raises(NotImplementedError):
        fetch.head("a")


#
# Fetch.run_task()
#


def test_fetch_run_task_with_meter(logger, mocker):
    meter_get = mocker.patch("download_latest.fetch.Meter.get")
    meter = meter_get.return_value
    meter.period.return_value = 0.22
    meter.begin.return_value = None
    meter.step.return_value = None
    meter.end.return_value = None

    task = mocker.MagicMock()
    task.loop.return_value = [0, 1, 2]
    task.wait.return_value = None
    fetch = Fetch(logger=logger, meter=True)

    fetch.run_task(task, path="abc", remote_size=123)
    meter_get.assert_called_once_with(path="abc", remote_size=123)
    assert meter.begin.call_count == 1
    assert meter.period.call_count == 1
    task.loop.assert_called_once_with(period=0.22)
    assert meter.step.call_count == 3
    assert meter.end.call_count == 1
    task.wait.assert_not_called()


def test_fetch_run_task_without_meter(logger, mocker):
    meter_get = mocker.patch("download_latest.fetch.Meter.get")
    task = mocker.MagicMock()
    task.loop.return_value = [0, 1, 2]
    task.wait.return_value = None
    fetch = Fetch(logger=logger)

    fetch.run_task(task)
    meter_get.assert_not_called()
    task.loop.assert_not_called()
    task.wait.assert_called_once_with()


#
# Fetch.get_fetch_class
#


# fmt: off
FETCH_CLASS_MATRIX = [
#    backend   curl   wget  fetch_class
    ("auto"  , True , True , FetchCurl  ),
    ("auto"  , True , False, FetchCurl  ),
    ("auto"  , False, True , FetchWget  ),
    ("auto"  , False, False, FetchPython),
    ("curl"  , True , True,  FetchCurl  ),
    ("curl"  , True , False, FetchCurl  ),
    ("curl"  , False, True,  FetchCurl  ),
    ("curl"  , False, False, FetchCurl  ),
    ("python", True , True,  FetchPython),
    ("python", True , False, FetchPython),
    ("python", False, True,  FetchPython),
    ("python", False, False, FetchPython),
    ("wget"  , True , True,  FetchWget  ),
    ("wget"  , True , False, FetchWget  ),
    ("wget"  , False, True,  FetchWget  ),
    ("wget"  , False, False, FetchWget  ),
]
# fmt: on


@pytest.mark.parametrize("backend,curl,wget,fetch_class", FETCH_CLASS_MATRIX)
def test_fetch_get_fetch_class(backend, curl, wget, fetch_class, mocker):
    mocker.patch("shutil.which", side_effect=[curl, wget])
    assert Fetch.get_fetch_class(backend) is fetch_class


#
# FetchCurl
#


#
# FetchCurl.convert_head_responses()
#


def test_fetch_curl_convert_head_responses_malformed(caplog, logger, mocker):
    fetch = FetchCurl(logger=logger, meter=None)
    task = mocker.MagicMock()
    task.args = ["curl", "..."]
    task.get.return_value = mocker.MagicMock(
        args=["curl", "..."],
        returncode=0,
        stdout=b"HTTP/1.1 !!! !!!\r\n  Content-Length: 123\r\n",
        stderr=b"",
    )

    responses = fetch.convert_head_responses("excite.ing", task)
    assert len(responses) == 0
    assert "cannot parse response" in caplog.text


def test_fetch_curl_convert_head_responses_ok(caplog, logger, mocker):
    fetch = FetchCurl(logger=logger, meter=False)
    task = mocker.MagicMock()
    task.args = ["curl", "..."]
    task.get.return_value = mocker.MagicMock(
        args=["curl", "..."],
        returncode=0,
        stdout=b"HTTP/1.1 200 OK\r\nContent-Length: 123\r\n",
        stderr=b"",
    )

    responses = fetch.convert_head_responses("work.ing", task)
    assert responses[0].headers["content-length"] == "123"
    assert "cannot parse response" not in caplog.text


#
# FetchCurl.get_*()
#


def test_fetch_wget_get_methods(logger):
    fetch = FetchCurl(logger=logger, meter=False)
    assert fetch.get_download_task_class() is FetchCurlDownloadTask
    assert fetch.get_head_task_class() is FetchCurlHeadTask
    assert fetch.get_name() == "curl"


#
# FetchCurlDownloadTask
#


def test_fetch_curl_download_task_with_resume():
    task = FetchCurlDownloadTask(
        url="https://example.com/",
        path="foo",
        resume=True,
    )

    assert task.args == [
        "curl",
        "--silent",
        "--show-error",
        "--fail",
        "--location",
        "--output",
        "foo",
        "--continue-at",
        "-",
        "--",
        "https://example.com/",
    ]


def test_fetch_curl_download_task_without_resume():
    task = FetchCurlDownloadTask(
        url="https://example.com/",
        path="foo",
        resume=False,
    )

    assert task.args == [
        "curl",
        "--silent",
        "--show-error",
        "--fail",
        "--location",
        "--output",
        "foo",
        "--",
        "https://example.com/",
    ]


#
# FetchCurlHeadTask
#


def test_fetch_curl_head_task():
    task = FetchCurlHeadTask("example.com")
    assert task.args == [
        "curl",
        "--silent",
        "--show-error",
        "--location",
        "--head",
        "--",
        "example.com",
    ]


#
# FetchPython
#


#
# FetchPython.download()
#


def test_fetch_python_download_error(caplog, logger, mocker):
    cls = mocker.patch("download_latest.fetch.FetchPythonDownloadTask")
    task = cls.return_value
    task.get.return_value = ("b", {})
    task.wait.side_effect = TaskUncaughtError(OSError("uh oh"))
    fetch = FetchPython(logger=logger, meter=None)

    assert fetch.download("a", "b", resume=True) is False
    cls.assert_called_once_with(url="a", path="b", resume=True)
    task.get.assert_not_called()
    task.wait.assert_called_once_with()

    assert "  python: download a -> b" in caplog.text
    assert "  python: error: OSError('uh oh')" in caplog.text
    assert "  python: download finished" in caplog.text


def test_fetch_python_download_ok(caplog, logger, mocker):
    cls = mocker.patch("download_latest.fetch.FetchPythonDownloadTask")
    task = cls.return_value
    task.get.return_value = ("b", {})
    task.wait.return_value = None
    fetch = FetchPython(logger=logger, meter=None)

    assert fetch.download("a", "b", resume=False) is True
    cls.assert_called_once_with(url="a", path="b", resume=False)
    task.get.assert_not_called()
    task.wait.assert_called_once_with()

    assert "  python: download a -> b" in caplog.text
    assert "  python: error" not in caplog.text
    assert "  python: download finished" in caplog.text


#
# FetchPython.head()
#


def test_fetch_python_head_error(caplog, logger, mocker):
    cls = mocker.patch("download_latest.fetch.FetchPythonHeadTask")
    task = cls.return_value
    task.get.return_value = None
    task.wait.side_effect = TaskUncaughtError(RuntimeError("woof"))
    fetch = FetchPython(logger=logger, meter=None)

    assert fetch.head("a") == []
    cls.assert_called_once_with(url="a")
    task.get.assert_not_called()
    task.wait.assert_called_once_with()

    assert "  python: head a" in caplog.text
    assert "  python: error: RuntimeError('woof')" in caplog.text
    assert "  python: head finished" in caplog.text


def test_fetch_python_head_no_response(caplog, logger, mocker):
    cls = mocker.patch("download_latest.fetch.FetchPythonHeadTask")
    task = cls.return_value
    task.get.return_value = []
    task.wait.return_value is None
    fetch = FetchPython(logger=logger, meter=None)

    assert fetch.head("a") == []
    cls.assert_called_once_with(url="a")
    task.get.assert_called()
    task.wait.assert_called_once_with()

    assert "  python: head a" in caplog.text
    assert "  python: error: no response received" in caplog.text
    assert "  python: head finished" in caplog.text


def test_fetch_python_head_ok(caplog, logger, mocker):
    headers = http.client.HTTPMessage()
    headers["content-length"] = "123"
    cls = mocker.patch("download_latest.fetch.FetchPythonHeadTask")
    task = cls.return_value
    task.get.return_value = [
        mocker.MagicMock(url="a", headers=headers, status=200, reason="OK")
    ]
    task.wait.return_value = None
    fetch = FetchPython(logger=logger, meter=None)

    responses = fetch.head("a")
    assert len(responses) == 1
    assert isinstance(responses[0], FetchResponse)
    assert responses[0].url == "a"
    assert responses[0].status == 200
    assert responses[0].reason == "OK"
    assert responses[0].headers["content-length"] == "123"

    cls.assert_called_once_with(url="a")
    task.get.assert_called()
    task.wait.assert_called_once_with()

    assert "  python: head a" in caplog.text
    assert "  python: error" not in caplog.text
    assert "  python: head finished" in caplog.text


#
# FetchPythonDownloadTask
#


def test_fetch_python_download_task_ok(mocker):
    task = FetchPythonDownloadTask("1.2.3/4", "5", resume=False)
    result = ("5", http.client.HTTPMessage())
    urlretrieve = mocker.patch("urllib.request.urlretrieve", return_value=result)

    assert task.run() is result
    urlretrieve.assert_called_once_with("http://1.2.3/4", filename="5")


def test_fetch_python_download_task_error(mocker):
    task = FetchPythonDownloadTask("1.2.3/4", "5", resume=False)
    mocker.patch("urllib.request.urlretrieve", side_effect=OSError("out of space"))

    with pytest.raises(OSError, match=r"^out of space$"):
        task.run()


#
# FetchPythonHeadTask
#
# These tests run real requests via httpserver because the logic is quite
# complicated to stub and mock.
#


def test_fetch_python_head_task_error(httpserver):
    httpserver.expect_request("/not-found").respond_with_data("not found", status=404)
    task = FetchPythonHeadTask(httpserver.url_for("/not-found"))
    responses = task.get()
    assert len(responses) == 1
    assert responses[0].url == httpserver.url_for("/not-found")
    assert responses[0].status == 404


def test_fetch_python_head_task_ok(httpserver):
    httpserver.expect_request("/welcome").respond_with_data("welcome", status=200)
    httpserver.expect_request("/hello").respond_with_data(
        "redirect", status=301, headers={"location": httpserver.url_for("/welcome")}
    )
    task = FetchPythonHeadTask(httpserver.url_for("/hello"))
    responses = task.get()
    assert len(responses) == 2
    assert responses[0].url == httpserver.url_for("/hello")
    assert responses[0].status == 301
    assert responses[1].url == httpserver.url_for("/welcome")
    assert responses[1].status == 200


#
# FetchSubProcess
#


#
# FetchSubProcess.download()
#


def test_fetch_subprocess_download_error_normal(caplog, logger, mocker):
    cls = mocker.MagicMock()
    task = cls.return_value
    task.args = ["prog", "..."]
    task.get.return_value = mocker.MagicMock(
        args=["prog", "..."],
        returncode=1,
        stdout=b"",
        stderr=b"",
    )
    task.wait.return_value = None
    fetch = FetchSubProcess(logger=logger, meter=False)
    mocker.patch.object(fetch, "get_download_task_class", return_value=cls)
    mocker.patch.object(fetch, "get_name", return_value="NAME")

    assert fetch.download("a", "b", resume=False) is False
    fetch.get_download_task_class.assert_called_once_with()
    fetch.get_name.assert_called()

    cls.assert_called_once_with(url="a", path="b", resume=False)
    task.get.assert_called()
    task.wait.assert_called_once_with()

    assert "    NAME: $ prog ..." in caplog.text
    assert "    NAME: NAME exited 1" in caplog.text
    assert "    NAME: code=1 out=b'' err=b''" in caplog.text


def test_fetch_curl_download_error_uncaught(caplog, logger, mocker):
    cls = mocker.MagicMock()
    task = cls.return_value
    task.args = ["prog", "..."]
    task.get.side_effect = TaskUncaughtError(OSError("toomuch"))
    task.wait.side_effect = TaskUncaughtError(OSError("toomuch"))
    fetch = FetchSubProcess(logger=logger, meter=None)
    mocker.patch.object(fetch, "get_download_task_class", return_value=cls)
    mocker.patch.object(fetch, "get_name", return_value="NAME")

    assert fetch.download("a", "b", resume=False) is False
    fetch.get_download_task_class.assert_called_once_with()
    fetch.get_name.assert_called()

    cls.assert_called_once_with(url="a", path="b", resume=False)
    task.get.assert_called()
    task.wait.assert_called_once_with()

    assert "    NAME: $ prog ..." in caplog.text
    assert "    NAME: error: OSError('toomuch')" in caplog.text
    assert "    NAME: code=? out=? err=?" in caplog.text


def test_fetch_curl_download_ok(caplog, logger, mocker):
    cls = mocker.MagicMock()
    task = cls.return_value
    task.args = ["prog", "..."]
    task.get.return_value = mocker.MagicMock(
        args=["prog", "..."],
        returncode=0,
        stdout=b"",
        stderr=b"",
    )
    task.wait.return_value = None
    fetch = FetchSubProcess(logger=logger, meter=None)
    mocker.patch.object(fetch, "get_download_task_class", return_value=cls)
    mocker.patch.object(fetch, "get_name", return_value="NAME")

    assert fetch.download("a", "b", resume=False) is True
    fetch.get_download_task_class.assert_called_once_with()
    fetch.get_name.assert_called()

    cls.assert_called_once_with(url="a", path="b", resume=False)
    task.get.assert_called()
    task.wait.assert_called_once_with()

    assert "    NAME: $ prog ..." in caplog.text
    assert "    NAME: error" not in caplog.text
    assert "    NAME: code=0 out=b'' err=b''" in caplog.text


#
# FetchSubProcess.head()
#


def test_fetch_subprocess_head_error_normal(caplog, logger, mocker):
    cls = mocker.MagicMock()
    task = cls.return_value
    task.args = ["prog", "..."]
    task.get.return_value = mocker.MagicMock(
        args=["prog", "..."],
        returncode=1,
        stdout=b"",
        stderr=b"",
    )
    task.wait.return_value = None
    fetch = FetchSubProcess(logger=logger, meter=False)
    mocker.patch.object(fetch, "get_head_task_class", return_value=cls)
    mocker.patch.object(fetch, "get_name", return_value="NAME")
    mocker.patch.object(fetch, "convert_head_responses", return_value=[])

    assert fetch.head("a") == []
    fetch.get_head_task_class.assert_called_once_with()
    fetch.get_name.assert_called()
    fetch.convert_head_responses.assert_not_called()

    cls.assert_called_once_with(url="a")
    task.get.assert_called()
    task.wait.assert_called_once_with()

    assert "    NAME: $ prog ..." in caplog.text
    assert "    NAME: NAME exited 1" in caplog.text
    assert "    NAME: code=1 out=b'' err=b''" in caplog.text


def test_fetch_subprocess_head_error_uncaught(caplog, logger, mocker):
    cls = mocker.MagicMock()
    task = cls.return_value
    task.args = ["prog", "..."]
    task.get.side_effect = TaskUncaughtError(OSError("toomuch"))
    task.wait.side_effect = TaskUncaughtError(OSError("toomuch"))
    fetch = FetchSubProcess(logger=logger, meter=None)
    mocker.patch.object(fetch, "get_head_task_class", return_value=cls)
    mocker.patch.object(fetch, "get_name", return_value="NAME")
    mocker.patch.object(fetch, "convert_head_responses", return_value=[])

    assert fetch.head("a") == []
    fetch.get_head_task_class.assert_called_once_with()
    fetch.get_name.assert_called()
    fetch.convert_head_responses.assert_not_called()

    cls.assert_called_once_with(url="a")
    task.get.assert_called()
    task.wait.assert_called_once_with()

    assert "    NAME: $ prog ..." in caplog.text
    assert "    NAME: error: OSError('toomuch')" in caplog.text
    assert "    NAME: code=? out=? err=?" in caplog.text


def test_fetch_subprocess_head_ok(caplog, logger, mocker):
    cls = mocker.MagicMock()
    task = cls.return_value
    task.args = ["prog", "..."]
    task.get.return_value = mocker.MagicMock(
        args=["prog", "..."],
        returncode=0,
        stdout=b"HTTP/1.0 200",
        stderr=b"",
    )
    task.wait.return_value = None
    fetch = FetchSubProcess(logger=logger, meter=None)
    mocker.patch.object(fetch, "get_head_task_class", return_value=cls)
    mocker.patch.object(fetch, "get_name", return_value="NAME")
    mocker.patch.object(fetch, "convert_head_responses", return_value=[])

    assert fetch.head("a") == []
    fetch.get_head_task_class.assert_called_once_with()
    fetch.get_name.assert_called()
    fetch.convert_head_responses.assert_called_once_with(url="a", task=task)

    cls.assert_called_once_with(url="a")
    task.get.assert_called()
    task.wait.assert_called_once_with()

    assert "    NAME: $ prog ..." in caplog.text
    assert "    NAME: error" not in caplog.text
    assert "    NAME: code=0 out=b'HTTP/1.0 200' err=b''" in caplog.text


#
# FetchSubProcess.check_process()
#


def test_fetch_subprocess_check_process_error_normal(caplog, logger, mocker):
    fetch = FetchSubProcess(logger=logger, meter=None)
    mocker.patch.object(fetch, "get_name", return_value="NAME")
    task = mocker.MagicMock()
    task.get.return_value = mocker.MagicMock(
        args=["prog", "..."],
        returncode=1,
        stdout=b"",
        stderr=b"ERROR ERROR",
    )
    assert fetch.check_process(task) is False
    assert "    NAME: ERROR ERROR" in caplog.text


def test_fetch_subprocess_check_process_error_silent(caplog, logger, mocker):
    fetch = FetchSubProcess(logger=logger, meter=None)
    mocker.patch.object(fetch, "get_name", return_value="NAME")
    task = mocker.MagicMock()
    task.get.return_value = mocker.MagicMock(
        args=["prog", "..."],
        returncode=1,
        stdout=b"",
        stderr=b"",
    )
    assert fetch.check_process(task) is False
    assert "    NAME: NAME exited 1" in caplog.text


def test_fetch_subprocess_check_process_ok(caplog, logger, mocker):
    fetch = FetchSubProcess(logger=logger, meter=None)
    task = mocker.MagicMock()
    task.get.return_value = mocker.MagicMock(
        args=["prog", "..."],
        returncode=0,
        stdout=b"",
        stderr=b"",
    )
    assert fetch.check_process(task) is True


#
# FetchSubProcess.log_*()
#


def test_fetch_log_begin(caplog, logger, mocker):
    fetch = FetchSubProcess(logger=logger, meter=None)
    mocker.patch.object(fetch, "get_name", return_value="NAME")
    task = mocker.MagicMock()
    task.args = ["prog", 'm"m']
    task.get.return_value = mocker.MagicMock(
        args=["prog", 'm"m'],
        returncode=0,
        stdout=b"",
        stderr=b"",
    )
    fetch.log_begin(task)
    assert "    NAME: $ prog 'm\"m'" in caplog.text


def test_fetch_log_end(caplog, logger, mocker):
    fetch = FetchSubProcess(logger=logger, meter=None)
    mocker.patch.object(fetch, "get_name", return_value="NAME")
    task = mocker.MagicMock()
    task.get.return_value = mocker.MagicMock(
        args=["prog", 'm"m'],
        returncode=1,
        stdout=b"SPAM " * 10,
        stderr=b"ERROR " * 10,
    )
    fetch.log_end(task)
    assert "    NAME: code=1 out=b'...' err=b'ERROR" in caplog.text

    caplog.clear()
    task.get.return_value = mocker.MagicMock(
        args=["prog", 'm"m'],
        returncode=1,
        stdout=b"SPAM " * 6,
        stderr=b"ERROR " * 4,
    )
    fetch.log_end(task)
    assert "    NAME: code=1 out=b'SPAM" in caplog.text
    assert "ERROR ERROR ERROR ERROR '" in caplog.text


def test_fetch_log_error(caplog, logger, mocker):
    fetch = FetchSubProcess(logger=logger, meter=None)
    mocker.patch.object(fetch, "get_name", return_value="NAME")
    fetch.log_error("uh oh")
    assert "    NAME: error: uh oh" in caplog.text


#
# FetchWget
#


#
# FetchWget.convert_head_responses()
#


def test_fetch_wget_convert_head_responses_malformed(caplog, logger, mocker):
    fetch = FetchWget(logger=logger, meter=False)
    task = mocker.MagicMock()
    task.args = ["wget", "..."]
    task.get.return_value = mocker.MagicMock(
        args=["wget", "..."],
        returncode=0,
        stdout=b"",
        stderr=b"  HTTP/1.1 !!! !!!\n  Content-Length: 123\n",
    )

    responses = fetch.convert_head_responses("excite.ing", task)
    assert responses == []
    assert "cannot parse response" in caplog.text


def test_fetch_wget_convert_head_responses_ok(caplog, logger, mocker):
    fetch = FetchWget(logger=logger, meter=False)
    task = mocker.MagicMock()
    task.args = ["wget", "..."]
    task.get.return_value = mocker.MagicMock(
        args=["wget", "..."],
        returncode=0,
        stdout=b"",
        stderr=b"  HTTP/1.1 301 \n  Location: https://a.b/c\n  HTTP/1.1 200 OK\n",
    )

    responses = fetch.convert_head_responses("work.ing", task)
    assert len(responses) == 2
    assert "cannot parse response" not in caplog.text


#
# FetchWget.get_*_methods()
#


def test_fetch_wget_get_task_class(logger):
    fetch = FetchWget(logger=logger, meter=False)
    assert fetch.get_download_task_class() is FetchWgetDownloadTask
    assert fetch.get_head_task_class() is FetchWgetHeadTask
    assert fetch.get_name() == "wget"


#
# FetchWgetDownloadTask
#


def test_fetch_wget_download_task_with_resume():
    task = FetchWgetDownloadTask(
        url="https://example.com/",
        path="foo",
        resume=True,
    )

    assert task.args == [
        "wget",
        "--quiet",
        "--output-document",
        "foo",
        "--continue",
        "--",
        "https://example.com/",
    ]


def test_fetch_wget_download_task_without_resume():
    task = FetchWgetDownloadTask(
        url="https://example.com/",
        path="foo",
        resume=False,
    )

    assert task.args == [
        "wget",
        "--quiet",
        "--output-document",
        "foo",
        "--",
        "https://example.com/",
    ]


#
# FetchWgetHeadTask
#


def test_fetch_wget_head_task():
    task = FetchWgetHeadTask("example.com")
    assert task.args == [
        "wget",
        "--quiet",
        "--server-response",
        "--spider",
        "--",
        "example.com",
    ]
