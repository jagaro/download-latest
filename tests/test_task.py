from __future__ import annotations

import pytest
import subprocess
import threading
from typing import Callable

from download_latest.task import Task, SubProcessTask, TaskTimeout, TaskUncaughtError

from helpers import *  # noqa: F403


A_TIME_INTERVAL = 0.005

#
# Task
#


class ExampleTask(Task[float]):
    def __init__(self, a: int, b: int) -> None:
        super().__init__()
        self.a, self.b = a, b
        self.pause = threading.Event()

    def run(self) -> float:
        self.pause.wait()
        return float(self.a / self.b)

    def unpause(self) -> None:
        self.pause.set()
        self.join()


@pytest.fixture
def task() -> ExampleTask:
    return ExampleTask(3, 2)


#
# Task.get()
#


def test_get_non_zero_timeout(task):
    task.start()
    with pytest.raises(TaskTimeout, match=f"^{A_TIME_INTERVAL}s elapsed$"):
        task.get(A_TIME_INTERVAL)

    task.unpause()

    assert task.get(A_TIME_INTERVAL) == 1.5


def test_get_uncaught_error(task):
    task.b = 0
    task.start()
    task.unpause()
    with pytest.raises(
        TaskUncaughtError, match=r"^ZeroDivisionError\('division by zero'\)$"
    ):
        task.get()


def test_get_zero_timeout(task):
    task.start()
    with pytest.raises(TaskTimeout, match=r"^0s elapsed$"):
        task.get(0)

    task.unpause()

    assert task.get(0) == 1.5


#
# Task.loop()
#


def test_task_loop_basic(task):
    beans = []
    for i in task.loop(period=A_TIME_INTERVAL):
        beans.append("fava")
        if i == 2:
            task.unpause()

    assert 3 <= len(beans) and len(beans) <= 4
    assert task.get() == 1.5


def test_task_loop_finished_before_loop(task):
    task.start()
    task.unpause()
    task.wait()
    assert task.running() is False

    beans = []
    for i in task.loop():
        beans.append("garbanzo")
    assert len(beans) == 1


#
# Task.running()
#


def test_task_running(task):
    assert task.running() is False

    task.start()
    assert task.running() is True

    task.unpause()

    assert task.running() is False


#
# Task.wait()
#


def test_task_wait(task):
    with pytest.raises(TaskTimeout, match=r"^0s elapsed$"):
        task.wait(0)

    task.unpause()

    assert task.wait(0) is None


#
# SubProcessTask
#


@pytest.fixture
def patch_subprocess_task(mocker) -> Callable:
    def patch_subprocess_task(
        task: SubProcessTask,
        returncode: int = 0,
        stdout: bytes = b"",
        stderr: bytes = b"",
        exception: Exception | None = None,
    ) -> SubProcessTask:
        def run(args, *extraargs, **kwargs) -> subprocess.CompletedProcess:
            if exception:
                raise exception
            else:
                return subprocess.CompletedProcess(
                    args=task.args,
                    returncode=returncode,
                    stdout=stdout,
                    stderr=stderr,
                )

        mock_run = mocker.patch.object(subprocess, "run", side_effect=run)
        return mock_run

    return patch_subprocess_task


def test_subprocess_task_error(patch_subprocess_task):
    task = SubProcessTask("bad-cmd")
    patch_subprocess_task(task, returncode=1, stderr=b"error")

    process = task.get()
    assert process.args == "bad-cmd"
    assert process.returncode == 1
    assert process.stdout == b""
    assert process.stderr == b"error"


def test_subprocess_task_exception(patch_subprocess_task):
    task = SubProcessTask(["no-such-cmd"])
    patch_subprocess_task(task, exception=FileNotFoundError("not found"))

    with pytest.raises(TaskUncaughtError, match=r"^FileNotFoundError\('not found'\)$"):
        task.get()


def test_subprocess_task_ok(patch_subprocess_task):
    task = SubProcessTask("cmd")
    patch_subprocess_task(task, returncode=0, stdout=b"hi")

    process = task.get()
    assert process.args == "cmd"
    assert process.returncode == 0
    assert process.stdout == b"hi"
    assert process.stderr == b""


def test_subprocess_task_real():
    task = SubProcessTask(["hostname"])

    process = task.get()
    assert process.args == ["hostname"]
    assert process.returncode == 0
    assert len(process.stdout) > 0
    assert process.stderr == b""
