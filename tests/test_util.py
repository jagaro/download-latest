from __future__ import annotations

import email.message
import os
import pathlib
import pytest
import time

from download_latest.util import (
    HumanNumberFormatter,
    deduce_filename_from_url,
    get_file_md5,
    get_file_modified,
    get_file_size,
    get_human_args,
    get_human_file_size,
    get_human_time,
    get_user_cache_dir,
    parse_header_accept_ranges,
    parse_header_content_length,
    parse_header_content_md5,
    parse_header_etag,
    parse_header_last_modified,
    rm_f,
    sanitize_filename,
    truncate,
    urllib_safe_url,
)

from helpers import *  # noqa: F403


#
# HumanNumberFormatter
#


# fmt: off
HUMAN_NUMBER_FORMAT_TESTS = [
    (             0, 3,        "0B"),
    (           999, 3,      "999B"),
    (          1000, 3,      "1.0K"),
    (         10188, 3,      "9.9K"),
    (         10189, 3,       "10K"),
    (       1023487, 3,      "999K"),
    (       1023488, 3,      "1.0M"),
    (      10433331, 3,      "9.9M"),
    (      10433332, 3,       "10M"),
    (    1048051711, 3,      "999M"),
    (    1048051712, 3,      "1.0G"),
    (   10683731148, 3,      "9.9G"),
    (   10683731149, 3,       "10G"),
    ( 1073204953087, 3,      "999G"),
    ( 1073204953088, 3,      "1.0T"),
    (             0, 4,        "0B"),
    (          9999, 4,     "9999B"),
    (         10000, 4,      "9.8K"),
    (        102348, 4,     "99.9K"),
    (        102349, 4,      "100K"),
    (      10239487, 4,     "9999K"),
    (      10239488, 4,      "9.8M"),
    (     104805171, 4,     "99.9M"),
    (     104805172, 4,      "100M"),
    (   10485235711, 4,     "9999M"),
    (   10485235712, 4,      "9.8G"),
    (             0, 5,        "0B"),
    (          9999, 5,     "9999B"),
    (         10000, 5,     "9.77K"),
    (        102394, 5,    "99.99K"),
    (        102395, 5,    "100.0K"),
    (       1023948, 5,    "999.9K"),
    (       1023949, 5,     "1000K"),
    (      10239487, 5,     "9999K"),
    (      10239488, 5,     "9.77M"),
    (             0, 6,        "0B"),
    (          9999, 6,     "9999B"),
    (         10000, 6,     "9.77K"),
    (       1023994, 6,   "999.99K"),
    (       1023995, 6,   "1000.0K"),
    (      10239948, 6,   "9999.9K"),
    (      10239949, 6,     "9.77M"),
    (    1048570757, 6,   "999.99M"),
    (    1048570758, 6,   "1000.0M"),
    (   10485707571, 6,   "9999.9M"),
    (   10485707572, 6,     "9.77G"),
    (             0, 7,        "0B"),
    (          9999, 7,     "9999B"),
    (         10000, 7,     "9.77K"),
    (      10239994, 7,  "9999.99K"),
    (      10239995, 7,     "9.77M"),
    (   10485754757, 7,  "9999.99M"),
    (   10485754758, 7,     "9.77G"),
    (             0, 8,        "0B"),
    (          9999, 8,     "9999B"),
    (         10000, 8,     "9.77K"),
    (      10239994, 8,  "9999.99K"),
    (      10239995, 8,     "9.77M"),
    (   10485754757, 8,  "9999.99M"),
    (   10485754758, 8,     "9.77G"),
]
# fmt: on


def test_human_number_formatter_basics():
    formatter = HumanNumberFormatter(1)
    assert formatter.format(4200) == "4K"
    formatter = HumanNumberFormatter(1, 1)
    assert formatter.format(4200) == "4.1K"
    with pytest.raises(ValueError):
        HumanNumberFormatter.suggest(limit=-1, width=3)
    with pytest.raises(ValueError):
        HumanNumberFormatter.suggest(limit=10, width=2)


def test_human_number_formatter_format_battery():
    for n, width, expectation in HUMAN_NUMBER_FORMAT_TESTS:
        formatter = HumanNumberFormatter.suggest(limit=n, width=width)
        test = formatter.format(n)
        assert test == expectation, f"{n!r} {width!r} {expectation!r}"


def test_human_number_formatter_format_with_total():
    total = 10_000
    formatter = HumanNumberFormatter.suggest(limit=total, width=3)
    assert formatter.format_with_total(0_000, total) == "0.0/9.8K"
    assert formatter.format_with_total(2_000, total) == "2.0/9.8K"
    assert formatter.format_with_total(7_000, total) == "6.8/9.8K"
    assert formatter.format_with_total(total, total) == "9.8/9.8K"


def test_util_args_to_string():
    assert deduce_filename_from_url("") is None
    assert deduce_filename_from_url("example.com") is None
    assert deduce_filename_from_url("example.com/a") == "a"
    assert deduce_filename_from_url("example.com/a/") == "a"
    assert deduce_filename_from_url("http://example.com") is None
    assert deduce_filename_from_url("http://example.com/") is None
    assert deduce_filename_from_url("http://example.com/abc/123") == "123"
    assert deduce_filename_from_url("b.b/%E2%9D%A3") == "%E2%9D%A3"


def test_util_deduce_filename_from_url():
    assert deduce_filename_from_url("") is None
    assert deduce_filename_from_url("example.com") is None
    assert deduce_filename_from_url("example.com/a") == "a"
    assert deduce_filename_from_url("example.com/a/") == "a"
    assert deduce_filename_from_url("http://example.com") is None
    assert deduce_filename_from_url("http://example.com/") is None
    assert deduce_filename_from_url("http://example.com/abc/123") == "123"
    assert deduce_filename_from_url("b.b/%E2%9D%A3") == "%E2%9D%A3"

    assert deduce_filename_from_url("a.a/..") is None
    assert deduce_filename_from_url("a.a/../..") is None
    assert deduce_filename_from_url("a.a/a/b/..") == "a"
    assert deduce_filename_from_url("a.a/a/b/./c/../.") == "b"

    assert deduce_filename_from_url("http://example.com/...", "posix") == "..."
    assert deduce_filename_from_url("http://example.com/...", "nt") is None

    assert deduce_filename_from_url("d.d/?p=1&o=2#l=3", "posix") == "?p=1&o=2#l=3"
    assert deduce_filename_from_url("d.d/?p=1&o=2#l=3", "nt") == "_p=1&o=2#l=3"


def test_util_get_file_md5(tmp_cwd):
    a = tmp_cwd / "a"
    assert get_file_md5(a) is None
    a.open("w").write("a" * 100000)
    assert get_file_md5(a) == "1af6d6f2f682f76f80e606aeaaee1680"


def test_util_get_file_modified(tmp_cwd):
    a = tmp_cwd / "a"
    assert get_file_modified(a) is None
    a.open("w")
    assert get_file_modified(a) >= int(time.time()) - 2  # fuzzy but good enough


def test_util_get_file_size(tmp_cwd):
    a = tmp_cwd / "a"
    assert get_file_size(a) is None
    a.open("w").write("a" * 1234)
    assert get_file_size(a) == 1234


def test_util_get_human_args():
    assert get_human_args(None) == ""
    assert get_human_args([]) == ""
    assert get_human_args(["ls", "-al"]) == "ls -al"
    assert get_human_args(["mv", '$"$', ".$."]) == "mv '$\"$' '.$.'"
    assert get_human_args(("sed", "des")) == "sed des"
    assert get_human_args(b"touch '-\n-'") == "touch '-\n-'"
    assert get_human_args('echo "hi there"') == 'echo "hi there"'


def test_util_get_human_file_size():
    assert get_human_file_size(40) == "40B"
    assert get_human_file_size(1200) == "1.2K"
    assert get_human_file_size(1010101) == "986K"
    assert get_human_file_size(1111111) == "1.1M"
    assert get_human_file_size(999999999) == "954M"
    assert get_human_file_size(9999999999) == "9.3G"
    assert get_human_file_size(11111111111) == "10G"


def test_util_get_human_time():
    assert get_human_time(1) == "1970-01-01T00:00:01Z (1)"
    assert get_human_time(946684800) == "2000-01-01T00:00:00Z (946684800)"
    assert get_human_time(4102444800) == "2100-01-01T00:00:00Z (4102444800)"
    assert get_human_time(1111111111111) == "XXXX-XX-XXTXX:XX:XXZ (1111111111111)"


#
# get_user_cache_dir()
#


@pytest.fixture
def gucd():
    return lambda: str(get_user_cache_dir())


@pytest.fixture
def posix(mocker) -> None:
    if os.name == "nt":  # pragma: no cover
        mocker.patch.object(pathlib, "PosixPath", pathlib.WindowsPath)
        mocker.patch.object(pathlib, "PurePosixPath", pathlib.PureWindowsPath)
    mocker.patch.object(os, "name", "posix")
    mocker.patch.dict(
        os.environ,
        {
            "HOME": "/home/bob",
            "LOCALAPPDATA": "",
            "XDG_CACHE_HOME": "/home/bob/.cache",
        },
    )


@pytest.fixture
def windows(mocker) -> None:
    if os.name != "nt":
        mocker.patch.object(pathlib, "WindowsPath", pathlib.PosixPath)
        mocker.patch.object(pathlib, "PureWindowsPath", pathlib.PurePosixPath)
    mocker.patch.object(os, "name", "nt")
    mocker.patch.dict(
        os.environ,
        {
            "HOME": "C:/Users/Bob",
            "LOCALAPPDATA": "C:/Users/Bob/AppData/Local",
            "XDG_CACHE_HOME": "",
        },
    )


def test_util_windows_get_user_cache_dir_with_localappdata(windows, gucd):
    assert gucd() == "C:/Users/Bob/AppData/Local/download-latest/Cache"


def test_util_windows_get_user_cache_dir_without_localappdata(windows, gucd, mocker):
    mocker.patch.dict(os.environ, {"LOCALAPPDATA": ""})
    assert gucd() == "C:/Users/Bob/AppData/Local/download-latest/Cache"


def test_util_windows_get_user_cache_dir_with_custom_localappdata(
    windows, gucd, mocker
):
    mocker.patch.dict(os.environ, {"LOCALAPPDATA": "D:/Data"})
    assert gucd() == "D:/Data/download-latest/Cache"


def test_util_posix_get_user_cache_dir_with_xdg_cache_home(posix, gucd):
    assert gucd() == "/home/bob/.cache/download-latest"


def test_util_posix_get_user_cache_dir_without_xdg_cache_home(posix, gucd, mocker):
    mocker.patch.dict(os.environ, {"XDG_CACHE_HOME": ""})
    assert gucd() == "/home/bob/.cache/download-latest"


def test_util_posix_get_user_cache_dir_with_custom_xdg_cache_home(posix, gucd, mocker):
    mocker.patch.dict(os.environ, {"XDG_CACHE_HOME": "/tmp/xdg"})
    assert gucd() == "/tmp/xdg/download-latest"


#
# parse_header_*()
#


@pytest.fixture
def headers() -> email.message.Message:
    return email.message.Message()


def test_util_parse_header_accept_ranges(headers):
    assert parse_header_accept_ranges(headers) is False
    headers["Accept-Ranges"] = "bytes"
    assert parse_header_accept_ranges(headers) is True
    del headers["Accept-Ranges"]
    headers["Accept-Ranges"] = "none"
    assert parse_header_accept_ranges(headers) is False


def test_util_parse_header_content_length(headers):
    assert parse_header_content_length(headers) is None
    headers["Content-Length"] = "123"
    assert parse_header_content_length(headers) == 123
    del headers["Content-Length"]
    headers["Content-Length"] = "   456    "
    assert parse_header_content_length(headers) == 456
    del headers["Content-Length"]
    headers["Content-Length"] = "boo123fortyfive"
    assert parse_header_content_length(headers) is None


def test_util_parse_header_content_md5(headers):
    assert parse_header_content_md5(headers) is None
    headers["Content-MD5"] = "b"
    assert parse_header_content_md5(headers) is None
    del headers["Content-MD5"]
    headers["Content-MD5"] = "bird"
    assert parse_header_content_md5(headers) is None
    del headers["Content-MD5"]
    headers["Content-MD5"] = "yHOO40C9FwhSod43oe2c+w=="
    assert parse_header_content_md5(headers) == "c8738ee340bd170852a1de37a1ed9cfb"


def test_util_parse_header_etag(headers):
    assert parse_header_etag(headers) is None
    headers["ETag"] = ""
    assert parse_header_etag(headers) is None
    del headers["ETag"]
    headers["ETag"] = "1234"
    assert parse_header_etag(headers) == "1234"


def test_util_parse_header_last_modified(headers):
    assert parse_header_last_modified(headers) is None
    headers["Last-Modified"] = "blah blah blah"
    assert parse_header_last_modified(headers) is None
    del headers["Last-Modified"]
    headers["Last-Modified"] = "Thu, 05 Oct 2023 13:03:06 GMT"
    assert parse_header_last_modified(headers) == 1696510986


#
# sanitize_filename()
#


def test_util_sanitize_filename_basics():
    assert sanitize_filename("") is None
    assert sanitize_filename(".") is None
    assert sanitize_filename("..") is None
    assert sanitize_filename("bar") == "bar"
    assert sanitize_filename("/etc/passwd") == "etcpasswd"
    assert sanitize_filename("\x001\x1e2\x1d") == "12"


def test_util_sanitize_filename_os_dependent():
    assert sanitize_filename("...", "posix") == "..."
    assert sanitize_filename("...", "nt") is None
    assert sanitize_filename("buz?a=<>#b\\az", "posix") == "buz?a=<>#b\\az"
    assert sanitize_filename("buz?a=<>#b\\az", "nt") == "buz_a=__#b_az"
    assert sanitize_filename(". . . xyz . . .", "posix") == ". . . xyz . . ."
    assert sanitize_filename(". . . xyz . . .", "nt") == ". . . xyz"


def test_util_sanitize_filename_unicode():
    unicode = "a" + ("🧑" * 60)
    assert len(unicode.encode("utf-8")) == 241
    sanitized = sanitize_filename(unicode)
    assert len(sanitized.encode("utf-8")) == 237


#
# ...
#


def test_util_rm_f(tmp_cwd):
    a = tmp_cwd / "a"
    b = tmp_cwd / "b"
    c = tmp_cwd / "c"
    a.open("w")
    b.mkdir()
    assert a.exists()
    assert b.exists()
    assert not c.exists()

    try:
        rm_f(a)
        rm_f(b)
        rm_f(c)
    except Exception:  # pragma: no cover
        assert False, "should not raise an exception"

    assert not a.exists()
    assert b.exists()
    assert not c.exists()


def test_util_truncate():
    assert truncate("abcdefghijklmnop", 6) == "abc..."
    assert truncate("abcdefghijklmnop", 3, "@") == "ab@"
    assert len(truncate("a" * 100)) == 80
    assert truncate("") == ""
    assert truncate("abcd", 4) == "abcd"
    assert truncate("abcd", 3) == "..."
    assert truncate("abcd", 2) == ".."
    assert truncate("abcd", 1) == "."
    assert truncate("abcd", 0) == ""
    assert truncate("abcd", -1) == "abcd"
    assert truncate(b"abcdefg", 4) == b"a..."
    assert truncate(b"abcdefg", 4, b"!!") == b"ab!!"


def test_util_urllib_safe_url():
    assert urllib_safe_url("a") == "http://a"
    assert urllib_safe_url("a/") == "http://a/"
    assert urllib_safe_url("a/b") == "http://a/b"
    assert urllib_safe_url("a/b/") == "http://a/b/"
    assert urllib_safe_url("/a") == "/a"
    assert urllib_safe_url("/a/") == "/a/"
    assert urllib_safe_url("http://a") == "http://a"
    assert urllib_safe_url("http://a.b/c") == "http://a.b/c"
    assert urllib_safe_url("https://a.b/c") == "https://a.b/c"
    assert urllib_safe_url("ftp://a.b/c") == "ftp://a.b/c"
