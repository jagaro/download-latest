from __future__ import annotations

import dataclasses
import logging
import os
from pathlib import Path
import pytest
from typing import Callable, Sequence

from download_latest.cache import Cache
from download_latest.cls import (
    Decision,
    DownloadLatest,
    DownloadLatestException,
    FileData,
    State,
)
from download_latest.fetch import Fetch, FetchException, FetchResponse
from download_latest.meta import DEFAULT_LOGGER

from helpers import *  # noqa: F403

# TODO add more tests


HEAD_ERROR_RAW = b"""\
HTTP/1.1 301 Moved Permanently\r
Location: https://example.com/blah\r
\r
HTTP/2 301 \r
location: https://www.example.com/blah\r
\r
HTTP/2 404 \r
content-type: text/html; charset=UTF-8\r
"""

HEAD_OK_RAW = b"""\
HTTP/1.1 301 Moved Permanently\r
Location: https://example.com/favicon.ico\r
\r
HTTP/2 301 \r
location: https://www.example.com/favicon.ico\r
\r
HTTP/2 200 \r
last-modified: Tue, 17 Sep 2019 16:54:06 GMT\r
content-length: 1776\r
"""

HEAD_MODERN_RAW = b"""\
HTTP/2 301 \r
location: https://m.m/moop\r
\r
HTTP/2 200 Welcome!\r
accept-ranges: bytes\r
content-md5: FHG8BwcbWTdFFLIyDgMi4Q==\r
content-length: 4\r
content-type: text/plain; charset=UTF-8\r
etag: "0123456789ABCDEF"\r
last-modified: Fri, 01 Jun 2018 04:50:22 GMT\r
"""


@pytest.fixture
def dl_factory(logger, mocker, tmp_cwd) -> Callable:
    def dl_factory(
        *args,
        responses: bytes | None = None,
        fetch_head_response: bytes | Exception | None = None,
        fetch_download_response: bytes | Exception | bool | None = None,
        **kwargs,
    ) -> DownloadLatest:
        def fetch_head(url: str) -> Sequence[FetchResponse]:
            assert dl.state is State.HEAD_BEGIN
            if isinstance(fetch_head_response, bytes):
                return FetchResponse.make_from_raw_multiple_headers(
                    url=url,
                    raw_multiple_headers=fetch_head_response,
                )
            elif isinstance(fetch_head_response, Exception):
                raise fetch_head_response
            else:
                return []

        def fetch_download(
            url: str,
            path: os.PathLike | str,
            remote_size: int | None = None,
            resume: bool = False,
        ) -> bool:
            assert dl.state is State.DOWNLOAD_BEGIN
            if isinstance(fetch_download_response, Exception):
                raise fetch_download_response
            elif isinstance(fetch_download_response, bytes):
                Path(path).open("wb").write(fetch_download_response)
            elif isinstance(fetch_download_response, bool):
                return fetch_download_response
            return True

        kwargs["logger"] = logger
        dl = DownloadLatest(*args, **kwargs)
        dl.cache.cache_dir = tmp_cwd / "dl-cache"

        if responses:
            dl.responses = FetchResponse.make_from_raw_multiple_headers(
                url=dl.url,
                raw_multiple_headers=responses,
            )

        mocker.patch.object(dl.fetch, "head", side_effect=fetch_head)
        mocker.patch.object(dl.fetch, "download", side_effect=fetch_download)
        return dl

    return dl_factory


@pytest.fixture(autouse=True)
def set_caplog_level_to_debug(caplog):
    caplog.set_level(logging.DEBUG)


#
# DownloadLatest.__init__()
#


def test_dl_init_basics(logger, tmp_cwd):
    dl = DownloadLatest(
        "a.co/a/b",
        "c/d",
        dry_run=True,
        force=True,
        meter=True,
    )
    assert dl.url == "a.co/a/b"
    assert dl.file == Path("c/d")
    assert dl.local_output_path == Path("c/d").resolve()
    assert dl.local_download_path == Path("c/d.download").resolve()
    assert dl.local_new_path == Path("c/d.new").resolve()
    assert dl.dry_run is True
    assert dl.force is True
    assert isinstance(dl.cache, Cache)
    assert isinstance(dl.fetch, Fetch)
    assert dl.fetch.meter is True
    assert dl.logger is DEFAULT_LOGGER
    assert dl.decision is None
    assert dl.responses is None
    assert dl.state is State.HEAD_READY


def test_dl_init_deduced(dl_factory):
    a = Path("a")
    a.open("w").write("a")
    b = Path("b")
    b.mkdir()
    c = Path("c.new")
    c.mkdir()
    d = Path("d.download")
    d.mkdir()

    assert dl_factory("a/1")
    assert dl_factory("a/a")
    with pytest.raises(DownloadLatestException, match=r"^cannot deduce filename$"):
        dl_factory("a")
    with pytest.raises(DownloadLatestException, match=r"^file exists:"):
        dl_factory("a/b")
    with pytest.raises(DownloadLatestException, match=r"^file exists:"):
        dl_factory("a/c")
    with pytest.raises(DownloadLatestException, match=r"^file exists:"):
        dl_factory("a/d")


def test_dl_init_explicit(dl_factory):
    a = Path("a")
    a.open("w").write("a")
    b = Path("b")
    b.mkdir()
    c = Path("c.new")
    c.mkdir()
    d = Path("d.download")
    d.mkdir()

    assert dl_factory("a", "1")
    assert dl_factory("a", "a")
    with pytest.raises(DownloadLatestException, match=r"^filename is empty"):
        dl_factory("a", "/")
    with pytest.raises(DownloadLatestException, match=r"^file exists:"):
        dl_factory("a", "b")
    with pytest.raises(DownloadLatestException, match=r"^file exists:"):
        dl_factory("a", "c")
    with pytest.raises(DownloadLatestException, match=r"^file exists:"):
        dl_factory("a", "d")


#
# DownloadLatest.__repr__()
#


def test_dl_repr(dl_factory):
    dl = dl_factory("a/b")
    assert repr(dl) == "DownloadLatest('a/b', 'b')"


#
# DownloadLatest.decide()
#


@pytest.fixture
def dl_moop(dl_factory) -> DownloadLatest:
    dl = dl_factory("m.m/moop", responses=HEAD_MODERN_RAW)
    dl.state = State.HEAD_OK
    with dl.cache.update() as data:
        data["output"] = {
            str(Path("moop").resolve()): {
                "etag": '"0123456789ABCDEF"',
                "md5": "1471bc07071b59374514b2320e0322e1",
            }
        }
    return dl


@pytest.fixture
def dl_moop_resume(dl_moop) -> DownloadLatest:
    dl = dl_moop
    Path("moop.download").open("w").write("mo")
    with dl.cache.update() as data:
        data["download"] = {
            str(Path("moop.download").resolve()): {
                "url": "m.m/moop",
                "modified": 1527828622,
                "size": 4,
            }
        }
    return dl


@pytest.fixture
def dl_simple(dl_factory) -> DownloadLatest:
    dl = dl_factory("a.bc/def", responses=HEAD_OK_RAW)
    dl.state = State.HEAD_OK
    return dl


def test_dl_decide_modern_exists_etag_match(dl_moop):
    dl = dl_moop
    Path("moop").open("w").write("moop")
    del dl.responses[-1].headers["content-md5"]

    decision = dl.decide().decision
    assert decision.download is False
    assert decision.action.startswith("etag match, not downloading")


def test_dl_decide_modern_exists_etag_mismatch(dl_moop):
    dl = dl_moop
    Path("moop").open("w").write("moop")
    del dl.responses[-1].headers["content-md5"]
    del dl.responses[-1].headers["etag"]
    dl.responses[-1].headers["etag"] = '"123"'

    decision = dl.decide().decision
    assert decision.download is True
    assert decision.action.startswith("etag mismatch, downloading")


def test_dl_decide_modern_exists_md5_match(dl_moop):
    dl = dl_moop
    Path("moop").open("w").write("moop")

    decision = dl.decide().decision
    assert decision.download is False
    assert decision.action.startswith("md5 match, not downloading")


def test_dl_decide_modern_exists_md5_mismatch(dl_moop):
    dl = dl_moop
    Path("moop").open("w").write("moox")

    decision = dl.decide().decision
    assert decision.download is True
    assert decision.action.startswith("md5 mismatch, downloading")


def test_dl_decide_modern_new(dl_moop):
    dl = dl_moop

    decision = dl.decide().decision
    assert dataclasses.asdict(decision) == {
        "action": "downloading",
        "download": True,
        "local": {
            "etag": None,
            "md5": None,
            "modified": None,
            "resume": True,
            "size": None,
        },
        "remote": {
            "etag": '"0123456789ABCDEF"',
            "md5": "1471bc07071b59374514b2320e0322e1",
            "modified": 1527828622,
            "resume": True,
            "size": 4,
        },
        "restart": False,
        "resume": False,
        "resume_restart_reason": None,
        "resume_start_at": None,
    }


def test_dl_decide_modern_resume_mismatch(dl_moop_resume):
    dl = dl_moop_resume
    del dl.responses[-1].headers["content-length"]
    dl.responses[-1].headers["content-length"] = "5"

    decision = dl.decide().decision
    assert decision.download is True
    assert decision.action == "restarting download"
    assert decision.resume_start_at is None
    assert decision.resume_restart_reason == "cache mismatch, not resuming"


def test_dl_decide_modern_resume_ok(dl_moop_resume):
    dl = dl_moop_resume

    decision = dl.decide().decision
    assert decision.download is True
    assert decision.action == "resuming download from 2B"
    assert decision.resume_start_at == 2
    assert decision.resume_restart_reason == "cache match, resuming"


def test_dl_decide_simple_exists(dl_simple):
    dl = dl_simple
    Path("def").open("w").write("A" * 1776)
    os.utime(Path("def"), (1568739246, 1568739246))

    decision = dl.decide().decision
    assert decision.download is False
    assert decision.action == "size and modified match, not downloading"


def test_dl_decide_simple_exists_but_force(dl_simple):
    dl = dl_simple
    dl.force = True
    Path("def").open("w").write("A" * 1776)
    os.utime(Path("def"), (1568739246, 1568739246))

    decision = dl.decide().decision
    assert decision.download is True
    assert decision.action == "downloading (force enabled)"


def test_dl_decide_simple_mismatch(dl_simple):
    dl = dl_simple
    Path("def").open("w").write("A" * 1776)
    os.utime(Path("def"), (1568739245, 1568739245))

    decision = dl.decide().decision
    assert decision.download is True
    assert decision.action == "local / remote mismatch, downloading"


def test_dl_decide_simple_new(caplog, dl_factory):
    dl = dl_factory("a.bc/def", responses=HEAD_OK_RAW)
    dl.state = State.HEAD_OK

    decision = dl.decide().decision
    assert dl.state is State.DOWNLOAD_READY
    assert dataclasses.asdict(decision) == {
        "action": "downloading",
        "download": True,
        "local": {
            "etag": None,
            "md5": None,
            "modified": None,
            "resume": True,
            "size": None,
        },
        "remote": {
            "etag": None,
            "md5": None,
            "modified": 1568739246,
            "resume": False,
            "size": 1776,
        },
        "restart": False,
        "resume": False,
        "resume_restart_reason": None,
        "resume_start_at": None,
    }


def test_dl_decide_simple_resume(dl_factory):
    dl = dl_factory("a.bc/def", responses=HEAD_OK_RAW)
    dl.state = State.HEAD_OK
    Path("def.download").open("w").write("A")
    with dl.cache.update() as data:
        data["download"] = {
            str(Path("def.download").resolve()): {
                "url": "a.bc/def",
                "modified": 1568739246,
                "size": 1776,
            }
        }

    decision = dl.decide().decision
    assert dl.state is State.DOWNLOAD_READY
    assert decision.download is True
    assert decision.action == "restarting download"
    assert decision.resume_start_at is None
    assert decision.resume_restart_reason == "server resume not supported"


#
# DownloadLatest.download()
#


def test_dl_download_dry_run(dl_factory):
    dl = dl_factory("a.bc/def", dry_run=True, responses=HEAD_OK_RAW)
    dl.decision = Decision()
    dl.state = State.DOWNLOAD_READY

    dl.download()
    assert dl.state is State.DOWNLOAD_PASS
    assert not Path("def").exists()
    assert not Path("def.new").exists()
    assert not Path("def.download").exists()


def test_dl_download_error(dl_factory):
    dl = dl_factory(
        "a.bc/xyz",
        responses=HEAD_OK_RAW,
        fetch_download_response=FetchException("error"),
    )
    dl.decision = Decision()
    dl.state = State.DOWNLOAD_READY

    with pytest.raises(FetchException, match=r"^error$"):
        dl.download()
    assert dl.local_output_path == Path("xyz").resolve()
    assert dl.state is State.DOWNLOAD_ERROR
    assert not Path("xyz").exists()
    assert not Path("xyz.new").exists()
    assert not Path("xyz.download").exists()


def test_dl_download_no_response(dl_factory):
    dl = dl_factory("a.bc/def")
    dl.decision = Decision()
    dl.state = State.DOWNLOAD_READY

    with pytest.raises(DownloadLatestException, match=r"^no response received$"):
        dl.download()
    assert dl.state is State.DOWNLOAD_ABORTED


def test_dl_download_no_decision(dl_factory):
    dl = dl_factory("a.bc/def", responses=b"HTTP/1.1 200")
    dl.state = State.HEAD_OK

    with pytest.raises(DownloadLatestException, match=r"^no decision made$"):
        dl.download()
    assert dl.state is State.DOWNLOAD_ABORTED


def test_dl_download_fails(dl_factory):
    dl = dl_factory("a.bc/def", responses=HEAD_OK_RAW, fetch_download_response=False)
    dl.decision = Decision()
    dl.state = State.DOWNLOAD_READY

    with pytest.raises(DownloadLatestException, match=r"^download failed$"):
        dl.download()
    assert dl.state is State.DOWNLOAD_ERROR
    assert not Path("def").exists()
    assert not Path("def.new").exists()
    assert not Path("def.download").exists()


def test_dl_download_ok(dl_factory):
    dl = dl_factory("a.bc/def", responses=HEAD_OK_RAW, fetch_download_response=b"mud")
    dl.decision = Decision()
    dl.state = State.DOWNLOAD_READY

    dl.download()
    assert dl.state is State.DOWNLOAD_OK
    assert dl.local_output_path == Path("def").resolve()
    assert Path("def").open("r").read() == "mud"
    assert Path("def.new").exists()
    assert not Path("def.download").exists()


def test_dl_download_remote_size_mismatch_larger(caplog, dl_factory):
    dl = dl_factory("m.m/moop", responses=HEAD_OK_RAW, fetch_download_response=b"moopy")
    dl.decision = Decision(
        remote=FileData(
            size=4,
        ),
    )
    dl.state = State.DOWNLOAD_READY

    dl.download()
    assert dl.state is State.DOWNLOAD_OK
    assert Path("moop").open("r").read() == "moopy"
    assert not Path("moop.download").exists()
    assert Path("moop.new").exists()

    assert "warning: size mismatch" in caplog.text
    assert "probably okay" in caplog.text


def test_dl_download_remote_size_mismatch_smaller(caplog, dl_factory):
    dl = dl_factory("m.m/moop", responses=HEAD_OK_RAW, fetch_download_response=b"moo")
    dl.decision = Decision(
        remote=FileData(
            size=4,
        ),
    )
    dl.state = State.DOWNLOAD_READY

    with pytest.raises(DownloadLatestException, match=r"^size mismatch"):
        dl.download()
    assert dl.state is State.DOWNLOAD_ERROR
    assert not Path("moop").exists()
    assert Path("moop.download").open("r").read() == "moo"
    assert not Path("moop.new").exists()


def test_dl_download_resume_ok(dl_factory):
    dl = dl_factory(
        "m.m/moop", responses=HEAD_MODERN_RAW, fetch_download_response=b"moop"
    )
    Path("moop.download").open("w").write("mo")
    dl.decision = Decision(
        resume=True,
        resume_start_at=2,
        remote=FileData(
            etag='"0123456789ABCDEF"',
            md5="1471bc07071b59374514b2320e0322e1",
            modified=1527828622,
            resume=True,
            size=4,
        ),
    )
    dl.state = State.DOWNLOAD_READY

    dl.download()
    assert dl.state is State.DOWNLOAD_OK
    assert Path("moop").open("r").read() == "moop"
    assert not Path("moop.download").exists()
    assert Path("moop.new").exists()


def test_dl_download_resume_mismatch(dl_factory):
    dl = dl_factory(
        "m.m/moop", responses=HEAD_MODERN_RAW, fetch_download_response=b"moot"
    )
    Path("moop.download").open("w").write("mo")
    dl.decision = Decision(
        resume=True,
        resume_start_at=2,
        remote=FileData(
            md5="1471bc07071b59374514b2320e0322e1",
            modified=1527828622,
            resume=True,
            size=4,
        ),
    )
    dl.state = State.DOWNLOAD_READY

    with pytest.raises(DownloadLatestException, match=r"^md5 mismatch"):
        dl.download()
    assert dl.state is State.DOWNLOAD_ERROR
    assert not Path("moop").exists()
    assert Path("moop.download").open("r").read() == "moot"
    assert not Path("moop.new").exists()


#
# DownloadLatest.head()
#


def test_dl_head_exception(dl_factory):
    dl = dl_factory("over", "there", fetch_head_response=Exception("error"))

    with pytest.raises(Exception, match=r"^error$"):
        dl.head()
    assert dl.state is State.HEAD_ERROR


def test_dl_head_no_response(dl_factory):
    dl = dl_factory("nowhere", "to-be-found", fetch_head_response=None)

    with pytest.raises(DownloadLatestException, match=r"^no response received$"):
        dl.head()
    assert dl.state is State.HEAD_ERROR


def test_dl_head_404(caplog, dl_factory):
    dl = dl_factory("example.com/blah", fetch_head_response=HEAD_ERROR_RAW)

    with pytest.raises(DownloadLatestException, match=r"^server returned error: 404$"):
        dl.head()
    assert dl.state is State.HEAD_ERROR
    dl.fetch.head.assert_called_once_with("example.com/blah")
    assert "head 1: [301] example.com/blah" in caplog.text
    assert "head 2: [301] https://example.com/blah" in caplog.text
    assert "head 3: [404] https://www.example.com/blah" in caplog.text


def test_dl_head_ok(caplog, dl_factory):
    dl = dl_factory("example.com/favicon.ico", fetch_head_response=HEAD_OK_RAW)

    dl.head()
    assert dl.state is State.HEAD_OK
    dl.fetch.head.assert_called_once_with("example.com/favicon.ico")
    assert "head 1: [301] example.com/favicon.ico" in caplog.text
    assert "head 2: [301] https://example.com/favicon.ico" in caplog.text
    assert "head 3: [200] https://www.example.com/favicon.ico" in caplog.text


#
# DownloadLatest.log_decision()
#


def test_dl_log_decision_no_decision(caplog, dl_factory):
    dl = dl_factory("x.yz/123")

    with pytest.raises(DownloadLatestException, match=r"^no decision made$"):
        dl.log_decision()


def test_dl_log_decision(caplog, dl_factory):
    dl = dl_factory("x.yz/123")
    dl.decision = Decision(resume_restart_reason="its okay this time")
    dl.log_decision()
    assert "     md5:" in caplog.text
    assert "    etag:" in caplog.text
    assert "    size:" in caplog.text
    assert "modified:" in caplog.text
    assert "  resume:" in caplog.text
    assert "   action: downloading" in caplog.text
    assert "its okay this time" in caplog.text


def test_dl_log_decision_with_meter(caplog, dl_factory):
    for meter in (True, False):
        caplog.clear()
        dl = dl_factory("a/b", meter=meter)
        dl.decision = Decision()
        dl.log_decision()

        for level in (logging.DEBUG, logging.INFO):
            logs = "\n".join(
                map(
                    lambda t: t[2],
                    filter(lambda t: t[1] == level, caplog.record_tuples),
                )
            )
            if meter is True:
                if level is logging.DEBUG:
                    assert "action: downloading" in logs
                else:
                    assert "action: downloading" not in logs
            else:
                if level is logging.INFO:
                    assert "action: downloading" in logs
                else:
                    assert "action: downloading" not in logs


#
# DownloadLatest.log_paths()
#


def test_dl_log_paths(caplog, dl_factory):
    dl = dl_factory("x.yz/123")
    dl.log_paths()
    assert "     url: x.yz/123" in caplog.text
    assert "    file: 123" in caplog.text
    assert f"  output: {Path.cwd()}/123" in caplog.text
    assert f"download: {Path.cwd()}/123.download" in caplog.text
    assert f"     new: {Path.cwd()}/123.new" in caplog.text


#
# DownloadLatest.run()
#


def test_dl_run_ok(dl_factory):
    content = b"A" * 1776
    dl = dl_factory(
        "a.bc/def",
        fetch_head_response=HEAD_OK_RAW,
        fetch_download_response=content,
    )
    assert dl.state is State.HEAD_READY

    dl.run()
    assert dl.state is State.DOWNLOAD_OK
    assert Path("def").open("rb").read() == content
    assert Path("def.new").exists()
    assert not Path("def.download").exists()

    dl.run()
    assert dl.state is State.DOWNLOAD_PASS
    assert Path("def").open("rb").read() == content
    assert not Path("def.new").exists()
    assert not Path("def.download").exists()


def test_dl_run_real(httpserver, logger, tmp_cwd):
    httpserver.expect_request("/file.txt").respond_with_data(
        "a" * 100,
        status=200,
        headers={"last-modified": "Sun, 15 Oct 2023 00:11:22 GMT"},
    )
    dl = DownloadLatest(
        url=httpserver.url_for("/file.txt"),
        backend="python",
        logger=logger,
    )

    dl.run()
    assert dl.state is State.DOWNLOAD_OK
    assert Path("file.txt").open("r").read() == "a" * 100
    assert Path("file.txt.new").exists()
    assert not Path("file.txt.download").exists()

    dl.run()
    assert dl.state is State.DOWNLOAD_PASS
    assert Path("file.txt").open("r").read() == "a" * 100
    assert not Path("file.txt.new").exists()
    assert not Path("file.txt.download").exists()
