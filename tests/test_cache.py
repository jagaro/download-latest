from __future__ import annotations

import logging
from pathlib import Path
import pytest
from typing import Generator

from download_latest.cache import Cache

from helpers import *  # noqa: F403


@pytest.fixture
def cache(tmp_cwd, logger) -> Generator:
    """Returns a new Cache and asserts that it is clean/reset."""
    cache = Cache(cache_dir=tmp_cwd, logger=logger, lock_timeout=0.01)
    assert cache.cache_dir.is_dir()
    assert not cache.cache_path.exists()
    assert not cache.lock_path.exists()
    assert cache.data == {
        "download": {},
        "output": {},
    }
    yield cache


@pytest.fixture(autouse=True)
def set_caplog_level_to_debug(caplog):
    caplog.set_level(logging.DEBUG)


#
# Cache.__init__
#


def test_cache_init(logger, tmp_cwd):
    cache = Cache(cache_dir=tmp_cwd / "123", logger=logger, lock_timeout=22)
    assert isinstance(cache.cache_dir, Path)
    assert isinstance(cache.cache_path, Path)
    assert isinstance(cache.lock_path, Path)
    assert cache.logger == logger
    assert cache.lock_timeout == 22
    assert cache.cache_path.name == "cache.json"
    assert cache.lock_path.name == "cache.lock"
    assert not cache.cache_dir.exists()
    assert not cache.cache_path.exists()
    assert not cache.lock_path.exists()
    assert cache.data == {
        "download": {},
        "output": {},
    }


#
# Cache.load()
#


def test_cache_load_basics(cache, caplog):
    cache.load()
    assert not Path("cache.lock").exists()
    assert cache.data == {"download": {}, "output": {}}
    assert "cache read error" not in caplog.text


def test_cache_load_dict(cache, caplog, json_write):
    json_write("cache.json", {"a": {}, "download": 2, "output": {"foo": 1}})
    cache.load()
    assert not Path("cache.lock").exists()
    assert cache.data == {"a": {}, "download": {}, "output": {"foo": 1}}
    assert "cache read error" not in caplog.text


def test_cache_load_number(cache, caplog, json_write):
    json_write("cache.json", 123)
    cache.load()
    assert not Path("cache.lock").exists()
    assert cache.data == {"download": {}, "output": {}}
    assert "cache read error" not in caplog.text


def test_cache_load_illegal(cache, caplog):
    Path("cache.json").open("w").write("xyz")
    cache.load()
    assert not Path("cache.lock").exists()
    assert cache.data == {"download": {}, "output": {}}
    assert "cache read error" not in caplog.text


def test_cache_load_os_error(cache, caplog, mocker):
    cache.data["download"]["a"] = 1
    assert cache.data == {"download": {"a": 1}, "output": {}}
    assert "cache read error" not in caplog.text
    mocker.patch.object(cache, "_read_json", side_effect=OSError())
    cache.load()
    assert "cache read error" in caplog.text
    assert cache.data == {"download": {"a": 1}, "output": {}}


#
# Cache.lock()
#


def test_lock_basics(cache, caplog):
    assert not Path("cache.lock").exists()
    with cache.lock():
        assert Path("cache.lock").exists()
    assert not Path("cache.lock").exists()
    assert "cache lock detected" not in caplog.text
    assert "cache lock error" not in caplog.text
    assert "cache lock timeout" not in caplog.text


def test_lock_lock_exists(cache, caplog):
    Path("cache.lock").open("w")
    with cache.lock():
        assert Path("cache.lock").exists()
    assert not Path("cache.lock").exists()
    assert "cache lock detected" in caplog.text
    assert "cache lock error" not in caplog.text
    assert "cache lock timeout" in caplog.text


def test_lock_os_error(cache, caplog, mocker):
    mocker.patch.object(cache, "_open_exclusive", side_effect=OSError())
    with cache.lock():
        pass
    assert "cache lock detected" in caplog.text
    assert "cache lock error" in caplog.text
    assert "cache lock timeout" in caplog.text


#
# Cache.update()
#


def test_update_basics(cache, caplog, json_read):
    with cache.update():
        assert Path("cache.lock").exists()
    assert not Path("cache.lock").exists()
    assert json_read("cache.json") == {
        "download": {},
        "output": {},
    }
    assert "cache write error" not in caplog.text


def test_update_create_item(cache, caplog, json_read):
    with cache.update() as data:
        data["a"] = 1
    assert json_read("cache.json") == {
        "a": 1,
        "download": {},
        "output": {},
    }
    assert "cache write error" not in caplog.text


def test_update_create_subitem(cache, caplog, json_read, json_write):
    json_write("cache.json", {"a": 1})
    with cache.update() as data:
        data["download"] = {"foo": "bar"}
    assert json_read("cache.json") == {
        "a": 1,
        "download": {"foo": "bar"},
        "output": {},
    }
    assert "cache write error" not in caplog.text


def test_update_os_error(cache, caplog, json_read, mocker):
    assert "cache write error" not in caplog.text
    mocker.patch.object(cache, "_write_json", side_effect=OSError())
    with cache.update():
        pass
    assert "cache write error" in caplog.text


def test_update_update_subitems(cache, caplog, json_read, json_write):
    json_write(
        "cache.json",
        {
            "download": {"1": 1, "2": 2},
            "output": {"3": 3},
        },
    )
    with cache.update() as data:
        del data["download"]["1"]
        data["output"]["3"] = 6
    assert json_read("cache.json") == {
        "download": {"2": 2},
        "output": {"3": 6},
    }
    assert "cache write error" not in caplog.text
