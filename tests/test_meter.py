from __future__ import annotations

import io
import pytest

from download_latest.meter import (
    DownloadWithRemoteMeter,
    DownloadWithoutRemoteMeter,
    HeadMeter,
    Meter,
)

from helpers import *  # noqa: F403

#
# Meter
#


def test_meter():
    output = io.StringIO()
    meter = Meter(output=output)

    assert meter.period() > 1 / 60
    with pytest.raises(NotImplementedError):
        meter.begin()
    with pytest.raises(NotImplementedError):
        meter.step()
    with pytest.raises(NotImplementedError):
        meter.end()
    meter.write("abc")
    assert output.getvalue() == "abc"


def test_meter_get():
    assert isinstance(Meter.get(), HeadMeter)
    assert isinstance(Meter.get(path="a"), DownloadWithoutRemoteMeter)
    assert isinstance(Meter.get(path="a", remote_size=1), DownloadWithRemoteMeter)


#
# DownloadWithRemoteMeter
#


def test_download_with_total_meter(tmp_cwd):
    output = io.StringIO()
    path = tmp_cwd / "a"
    meter = DownloadWithRemoteMeter(output=output, path=path, remote_size=123)

    assert meter.begin() is None
    assert output.getvalue().startswith("\r\x1b[2K\r[\x1b")
    assert "%" in output.getvalue()
    assert meter.step() is None
    assert meter.end() is None


#
# DownloadWithoutRemoteMeter
#


def test_download_without_total_meter(tmp_cwd):
    output = io.StringIO()
    path = tmp_cwd / "a"
    meter = DownloadWithoutRemoteMeter(output=output, path=path)

    assert meter.begin() is None
    assert output.getvalue().startswith("\r\x1b[2K\r[\x1b")
    assert "%" not in output.getvalue()
    assert meter.step() is None
    assert meter.end() is None


#
# HeadMeter
#


def test_head_meter():
    output = io.StringIO()
    meter = HeadMeter(output=output)

    assert meter.begin() is None
    assert output.getvalue().startswith("\r\x1b[2K\r request")
    assert meter.step() is None
    assert meter.end() is None
