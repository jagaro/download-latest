from __future__ import annotations

from download_latest.spinner import Spinner


def test_spinner():
    s = Spinner(chars="123", width=5, step=1, index=6)
    assert s.chars == "123"
    assert s.width == 5
    assert s.step == 1
    assert s.index == 0
    assert iter(s) is s

    assert next(s) == "23123"
    assert s.index == 1
    assert next(s) == "31231"
    assert s.index == 2
    assert next(s) == "12312"
    assert s.index == 0
    assert next(s) == "23123"
    assert s.index == 1

    s.step = -1
    assert next(s) == "12312"
    assert next(s) == "31231"
    assert next(s) == "23123"
