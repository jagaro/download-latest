from __future__ import annotations

import re

from download_latest.meta import (
    DEFAULT_LOGGER,
    __description__,
    __program__,
    __version__,
)


def test_util_default_logger():
    assert DEFAULT_LOGGER.name == "download_latest"


def test_meta_values():
    assert __program__ == "download-latest"
    assert re.match(r"^\d+\.\d+\.\d+([a-zA-Z]\d+)?$", __version__)
    assert "Download" in __description__
