from __future__ import annotations

import json
import logging
import os
from pathlib import Path
import pytest
import secrets
from typing import Callable, Generator


@pytest.fixture
def json_read() -> Callable:
    def _json_read(path: os.PathLike | str) -> object:
        """Returns data parsed as JSON from path."""
        with Path(path).open("r", encoding="utf-8") as f:
            return json.load(f)

    return _json_read


@pytest.fixture
def json_write() -> Callable:
    def _json_write(path: os.PathLike | str, data: object) -> None:
        """Writes data as JSON to path."""
        with Path(path).open("w", encoding="utf-8") as f:
            json.dump(data, f)

    return _json_write


@pytest.fixture
def logger() -> logging.Logger:
    """Returns a logger with a random name."""
    return logging.getLogger("tests_helpers_logger_" + secrets.token_hex(16))


@pytest.fixture
def tmp_cwd(tmp_path: Path) -> Generator:
    """Create a temporary directory (with tmp_path) and chdir into it."""
    old_cwd = Path.cwd()
    try:
        os.chdir(tmp_path)
        yield tmp_path
    finally:
        os.chdir(old_cwd)
