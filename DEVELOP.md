# Download Latest

## Development Guide

### Setup

1.  Install [pyenv](https://github.com/pyenv/pyenv) and
    [Poetry](https://python-poetry.org/).

2.  Clone the repository:

    ```sh
    git clone git@gitlab.com:jagaro/download-latest.git
    cd download-latest
    ```

3.  Add the pre-commit hook:

    ```
    ln -srf .pre-commit .git/hooks/pre-commit
    ```

4.  Install Python versions:

    ```sh
    pyenv install -s
    ```

5.  Install Poetry dependencies:

    ```sh
    poetry install
    ```

    If you get an error, you may need to set the following:

    ```sh
    export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring
    ```

    See https://github.com/python-poetry/poetry/issues/1917.

### Common Tasks

| Command                                             | Description                            |
| --------------------------------------------------- | -------------------------------------- |
| `no-pip/generate`                                   | Generate `no-pip` version              |
| `poetry run pytest`                                 | Run unit tests                         |
| `poetry run ptw .`                                  | Test watcher                           |
| `poetry run nox -s lint`                            | Lint code                              |
| `poetry nox`                                        | Run full test suite                    |
| `poetry nox -R`                                     | Run full suite without rebuilding envs |
| `poetry run pytest --cov --cov-report=term-missing` | Brief coverage report                  |
| `poetry run pytest --cov --cov-report=html`         | HTML coverage report                   |
| `poetry env use 3.x`                                | Use a different Python                 |
| `poetry env remove --all`                           | Remove all Poetry venvs                |
| `git clean -fx .`                                   | Clean cached files                     |

### Tag New Version

1.  Set a new version:

    ```sh
    poetry version              # show current version
    poetry version NEW_VERSION  # set new version
    ```

2.  Commit, tag and push:

    ```sh
    VERSION="$(poetry version -s)"
    git commit -S -a -m "Bump version to $VERSION"
    git tag -s -m "Bump version to $VERSION" $VERSION
    git push --follow-tags
    ```

    If the tests pass, a new release is automatically created on
    [GitLab](https://gitlab.com/).

### Publish to PyPI

1.  Configure:

    ```sh
    poetry config repositories.test-pypi https://test.pypi.org/legacy/
    poetry config pypi-token.test-pypi TOKEN
    poetry config repositories.pypi https://pypi.org/legacy/
    poetry config pypi-token.pypi TOKEN
    ```

2.  Build:

    ```sh
    poetry build
    ```

3.  Publish to Test PyPI:

    ```sh
    poetry publish -r test-pypi
    ```

4.  Check the test PyPI Package:

    ```sh
    python3 -m venv check
    check/bin/pip install -i https://test.pypi.org/simple/ download-latest
    check/bin/download-latest -V
    rm -rf check
    ```

5.  Deploy to official PyPI:

    ```sh
    poetry publish
    ```

6.  Check the official PyPI Package:

    ```sh
    python3 -m venv check
    check/bin/pip install download-latest
    check/bin/download-latest -V
    rm -rf check
    ```
